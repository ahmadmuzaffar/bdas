package pk.gov.pitb.bdas.dialogues;


import android.content.Context;
import android.view.WindowManager;

import cn.pedant.SweetAlert.SweetAlertDialog;
import pk.gov.pitb.bdas.helperClasses.Globals;

public class SweetAlertDialogs {

    private static SweetAlertDialogs instance = null;

    private SweetAlertDialogs() {
    }

    public static SweetAlertDialogs getInstance() {
        if (instance == null) {
            instance = new SweetAlertDialogs();
        }
        return instance;
    }

    public void showDialogYesNo(Context context, String title, String message, OnDialogClickListener onClickPositiveButton, OnDialogClickListener onClickNegativeButton, boolean bIsCancelable) {
        try {
            SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE);
            if (title != null) {
                dialog.setTitle(title);
            }
            dialog.setContentText(message);
            dialog.setConfirmText("Yes");
            dialog.setConfirmClickListener(onClickPositiveButton);
            dialog.setCancelText("No");
            dialog.setCancelClickListener(onClickNegativeButton);
            dialog.setCancelable(bIsCancelable);
            dialog.show();
        } catch (WindowManager.BadTokenException e) {
            e.printStackTrace();
        }
    }

    public void showDialogOK(Context context, String title, String message, OnDialogClickListener onClickPositiveButton, boolean bIsCancelable) {
        try {
            SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE);

            if (title != null) {
                dialog.setTitleText(title);
            }
            if (message != null) {
                dialog.setContentText(message);
            }
            dialog.setConfirmClickListener(onClickPositiveButton);
            dialog.setCancelable(bIsCancelable);
            dialog.show();
        } catch (WindowManager.BadTokenException e) {
            e.printStackTrace();
        }
    }
    public void showDialogPermissionDenied(Context context, OnDialogClickListener onClickPositiveButton) {
        try {
            SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE);

                dialog.setTitleText("Permission Denied");

                dialog.setContentText("Allow all permissions to proceed.");

                dialog.setConfirmClickListener(onClickPositiveButton);
            dialog.show();
        } catch (WindowManager.BadTokenException e) {
            e.printStackTrace();
        }
    }

    public void showDialogOKCancel(Context context, String title, String message, OnDialogClickListener onClickPositiveButton, OnDialogClickListener onClickNegativeButton, boolean bIsCancelable) {
        try {
            SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE);
            if (title != null) {
                dialog.setTitle(title);
            }
            dialog.setContentText(message);
            dialog.setCancelText("Cancel");
            dialog.setCancelClickListener(onClickNegativeButton);
            dialog.setConfirmText("OK");
            dialog.setConfirmClickListener(onClickPositiveButton);
            dialog.setCancelable(bIsCancelable);
            dialog.show();
        } catch (WindowManager.BadTokenException e) {
            e.printStackTrace();
        }
    }

    public void showDialogOnBackPressed() {
        SweetAlertDialog dialog = new SweetAlertDialog(Globals.getUsage().mContext, SweetAlertDialog.WARNING_TYPE);
        dialog.setTitle("All Entered Data will be Lost");
        dialog.setContentText("Are You Sure You Want to Exit Application?");
        dialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                Globals.getUsage().mActivity.finish();
            }
        });
        dialog.setConfirmText("Yes");
        dialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {

            }
        });
        dialog.setCancelText("No");
        dialog.show();
    }

    public interface OnDialogClickListener extends SweetAlertDialog.OnSweetClickListener {
    }
}