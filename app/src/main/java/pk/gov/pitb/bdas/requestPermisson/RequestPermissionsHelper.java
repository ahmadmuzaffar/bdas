package pk.gov.pitb.bdas.requestPermisson;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

public class RequestPermissionsHelper {
    public static final int requestPermissionCode = 212;
    public static final int notShouldShowRequestPermissionRationaleCode = 213;
    public static final int notCheckSelfPermissionCode = 214;

    public static String[] persmissionsStringArray = new String[]
            {
                      Manifest.permission.WRITE_EXTERNAL_STORAGE
                    , Manifest.permission.READ_EXTERNAL_STORAGE
                    , Manifest.permission.ACCESS_FINE_LOCATION
                    , Manifest.permission.ACCESS_COARSE_LOCATION
            };


    public static Context mContext;
    public static Activity mActivity;

    public static int requestPermission(Activity activity) {
        mContext = activity;
        mActivity = activity;
        if (!RequestPermissionsHelper.checkSelfPermission(persmissionsStringArray)) {
            if (!RequestPermissionsHelper.shouldShowRequestPermissionRationale(persmissionsStringArray)) {
                return notShouldShowRequestPermissionRationaleCode; // show a dialog that permission denied
            } else {
                return requestPermissionCode; // request for permissions' grant
            }
        }else{
            return notCheckSelfPermissionCode; //if not self permission granted then initialize application
        }
    }
    private static boolean checkSelfPermission(String[] persmissionsStringArray){
        final int grant = PackageManager.PERMISSION_GRANTED;
        for(int i=0; i<persmissionsStringArray.length;i++){
            if (ContextCompat.checkSelfPermission(mContext, persmissionsStringArray[i]) != grant)
                return false;
        }
        return true;
    }

    private static boolean shouldShowRequestPermissionRationale(String[] persmissionsStringArray){
        final int grant = PackageManager.PERMISSION_GRANTED;
        for(int i=0; i<persmissionsStringArray.length;i++){
            if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity, persmissionsStringArray[i]))
                return false;
        }
        return true;
    }
}

// code to add in activity

//    public void requestPermission() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            // Android M Permission check
//            int permission = RequestPermissionsHelper.requestPermission();
//            if(permission == RequestPermissionsHelper.requestPermissionCode){
//                ActivityCompat.requestPermissions(mContext, RequestPermissionsHelper.persmissionsStringArray, PERMISSION_REQUEST_CODE);
//            }else if(permission == RequestPermissionsHelper.notShouldShowRequestPermissionRationaleCode){
//                showAlertDialogOK();
//            } if(permission == RequestPermissionsHelper.notCheckSelfPermissionCode) {
//                initializeApplication();
//            }
//        } else {
//            initializeApplication();
//        }
//    }
//
//    public void showAlertDialogOK(){
//        AlertDialogs.getInstance().showDialogOK(mContext, "Permission Denied", "Please allow all permissions in App Settings for additional functionality.", new AlertDialogs.OnDialogClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                finish();
//            }
//        }, false);
//
//    }


//@Override
//public void onRequestPermissionsResult(int requestCode, String permissions[],
//        int[] grantResults) {
//        switch (requestCode) {
//        case PERMISSION_REQUEST_CODE:
//        if (grantResults.length > 0){
//        boolean granted = true;
//        for(int i=0;i<grantResults.length;i++){
//        if(grantResults[i] != PackageManager.PERMISSION_GRANTED){
//        granted=false;
//        break;
//        }
//        }
//        if(granted){
//        initializeApplication();
//        }else{
//        AlertDialogs.getInstance().showDialogOK(this, "Permission Denied", "Allow all permissions to proceed.", new AlertDialogs.OnDialogClickListener() {
//@Override
//public void onClick(DialogInterface dialog, int which) {
//        finish();
//        }
//        }, false);
//        }
//        } else {
//        AlertDialogs.getInstance().showDialogOK(this, "Permission Denied", "Allow all permissions to proceed.", new AlertDialogs.OnDialogClickListener() {
//@Override
//public void onClick(DialogInterface dialog, int which) {
//        finish();
//        }
//        }, false);
//        }
//        break;
//        }
//        }
