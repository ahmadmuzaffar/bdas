package pk.gov.pitb.bdas.fragments.home;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.jaeger.library.StatusBarUtil;
import com.orm.SugarContext;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import pk.gov.pitb.bdas.DrawerActivity;
import pk.gov.pitb.bdas.R;
import pk.gov.pitb.bdas.databinding.FragmentHomeBinding;
import pk.gov.pitb.bdas.dialogues.SweetAlertDialogs;
import pk.gov.pitb.bdas.helperClasses.Globals;
import pk.gov.pitb.bdas.model.SubmitResponse;
import pk.gov.pitb.bdas.model.loginResponse.FormData;
import pk.gov.pitb.bdas.model.loginResponse.UserData;
import pk.gov.pitb.bdas.serverRetrofit.APIService;
import pk.gov.pitb.bdas.serverRetrofit.ApiUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeViewModel extends ViewModel {

    @SuppressLint("SimpleDateFormat")
    private DateFormat df = new SimpleDateFormat("dd/MM/yy");
    private Calendar calobj = Calendar.getInstance();


    FormData formDataObject;

    private FragmentHomeBinding binding;
    private Context context;
    private Activity activity;
    private ProgressDialog progressDialog;
    private APIService mAPIService;
    private Globals mGlobals;
    UserData userData;

    public HomeViewModel(Context context, FragmentHomeBinding binding, DrawerActivity activity) {
        this.context = context;
        this.binding = binding;
        this.activity = activity;

        userData = UserData.last(UserData.class);
        binding.tvUsername.setText(userData.getUserName());
        binding.tvEmail.setText(userData.getEmail());

        setInstances();
        listeners();
        checkingPreviousData();
    }

    private void setInstances(){
        mGlobals = Globals.getInstance(activity);
        SugarContext.init(context);
        ButterKnife.bind(activity);
        mAPIService = ApiUtils.getAPIService();
        StatusBarUtil.setTransparent(activity);
    }

    private void listeners() {

        textWatchers();

        binding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(validateFields()){
                    submit();
                }

            }
        });

    }

    private boolean validateFields() {
        formDataObject = new FormData();

        if(binding.etPendingPlans.length() == 0){
            binding.etPendingPlans.setError("Field Required");
            binding.etPendingPlans.requestFocus();
            return false;
        }if(binding.etNewPlans.length() == 0){
            binding.etNewPlans.setError("Field Required");
            binding.etNewPlans.requestFocus();
            return false;
        }if(binding.etApprovedPlans.length() == 0){
            binding.etApprovedPlans.setError("Field Required");
            binding.etApprovedPlans.requestFocus();
            return false;
        }

        formDataObject.setPendingBuilding(Integer.parseInt(binding.etPendingPlans.getText().toString()));
        formDataObject.setNewBuilding(Integer.parseInt(binding.etNewPlans.getText().toString()));
        formDataObject.setTotalBuilding(Integer.parseInt(binding.etTotalPlans.getText().toString()));
        formDataObject.setApprovedBuilding(Integer.parseInt(binding.etApprovedPlans.getText().toString()));
        formDataObject.setRemainingBuilding(Integer.parseInt(binding.etRemainingPlans.getText().toString()));

        return true;
    }

    private void submit() {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();


//        Gson gson = new Gson();
//        String jsonString = gson.toJson(formDataObject);
//        JsonParser parser = new JsonParser();
//        JsonElement json = (JsonElement) parser.parse(jsonString);
//        JsonObject jsonObject = json.getAsJsonObject();


        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("user_id", userData.getUserId());
            jsonObject.addProperty("user_token", userData.getAccessToken());
            jsonObject.addProperty("pending_building", formDataObject.getPendingBuilding());
            jsonObject.addProperty("new_building", formDataObject.getNewBuilding());
            jsonObject.addProperty("total_building", formDataObject.getTotalBuilding());
            jsonObject.addProperty("approved_building", formDataObject.getApprovedBuilding());
            jsonObject.addProperty("remaining_building", formDataObject.getRemainingBuilding());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("create json string",jsonObject.toString());


        Map<String, String> hashMap = new HashMap();
        hashMap.put("data", jsonObject.toString());
        hashMap.put("app_data", Globals.getUsage().getJSON().toString());


        mAPIService.saveBuilding("Bearer " + userData.getAccessToken(), hashMap)
                .enqueue(new Callback<SubmitResponse>() {
                    @Override
                    public void onResponse(Call<SubmitResponse> call, Response<SubmitResponse> response) {
                        progressDialog.dismiss();
                        if (response.isSuccessful()) {
                            Log.i("TAG", "post submitted to API." + response.body().toString());

                            SubmitResponse submitResponse = response.body();
                            if(submitResponse.getStatus()){
                                // success
//                                Globals.getUsage().sharedPreferencesEditor.setFirstTimeLogin(false);
                                if(userData.getRecordExist()==0) {
                                    userData.setRecordExist(1);
                                    userData.save();
                                }
                                FormData.deleteAll(FormData.class, "is_unsent = ?", ""+0);
                                formDataObject = submitResponse.getFormData();
                                formDataObject.setUnsent(false);
                                formDataObject.save();
                                SweetAlertDialogs.getInstance().showDialogOK(mGlobals.mContext, "Added!", submitResponse.getMessage(), new SweetAlertDialogs.OnDialogClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();
                                        openHomeActivityAgain();
                                    }
                                }, false);
                            }else{
                                SweetAlertDialogs.getInstance().showDialogOK(mGlobals.mContext, "Cannot Submit/Save", submitResponse.getMessage(), new SweetAlertDialogs.OnDialogClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();
                                        openHomeActivityAgain();
                                    }
                                }, false);
                            }
                        } else {
                            Globals.getUsage().showDialogFailure(Globals.getUsage().mContext.getResources().getString(R.string.try_later));
                        }
                    }

                    @Override
                    public void onFailure(Call<SubmitResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        Log.e("failure", "Unable to submit post to API.");

                        formDataObject.setUnsent(true);
                        formDataObject.save();
                        formDataObject = new FormData();
//                        Globals.getUsage().sharedPreferencesEditor.setFirstTimeLogin(false);

                        if(userData.getRecordExist()==0) {
                            userData.setRecordExist(1);
                            userData.save();
                        }

                        if (t instanceof IOException) {
                            SweetAlertDialogs.getInstance().showDialogOK(mGlobals.mContext, "Network Error",
                                    Globals.getUsage().mContext.getString(R.string.network_error_save), new SweetAlertDialogs.OnDialogClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();
                                    openHomeActivityAgain();
                                }
                            }, false);
                        }
                        else {
                            SweetAlertDialogs.getInstance().showDialogOK(mGlobals.mContext, "Error",
                                    Globals.getUsage().mContext.getString(R.string.error_message), new SweetAlertDialogs.OnDialogClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                            openHomeActivityAgain();
                                        }
                                    }, false);
                        }
                    }
                });
    }

    private void textWatchers() {
        binding.etPendingPlans.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int pendingPlanInteger = 0;
                int addedPlansInteger = 0;
                int approvedPlanInteger = 0;

                if(!s.toString().equals(""))
                    pendingPlanInteger = Integer.parseInt(s.toString());

                String addedPlanString = binding.etNewPlans.getText().toString();
                if(!addedPlanString.equals("")) {
                    addedPlansInteger = Integer.parseInt(addedPlanString);
                }

                String approvedPlanString = binding.etApprovedPlans.getText().toString();
                if(!approvedPlanString.equals("")) {
                    approvedPlanInteger = Integer.parseInt(approvedPlanString);
                }


                int totalPlans = pendingPlanInteger + addedPlansInteger;
                int remainingPlans = totalPlans - approvedPlanInteger;

                binding.etTotalPlans.setText(String.valueOf(totalPlans));

                binding.etRemainingPlans.setText(String.valueOf(remainingPlans));
            }
        });
        binding.etNewPlans.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int pendingPlanInteger = 0;
                int addedPlansInteger = 0;
                int approvedPlanInteger = 0;

                if(!s.toString().equals(""))
                    addedPlansInteger = Integer.parseInt(s.toString());

                String pendingPlanString = binding.etPendingPlans.getText().toString();
                if(!pendingPlanString.equals("")) {
                    pendingPlanInteger = Integer.parseInt(pendingPlanString);
                }

                String approvedPlanString = binding.etApprovedPlans.getText().toString();
                if(!approvedPlanString.equals("")) {
                    approvedPlanInteger = Integer.parseInt(approvedPlanString);
                }


                int totalPlans = pendingPlanInteger + addedPlansInteger;
                int remainingPlans = totalPlans- approvedPlanInteger;

                binding.etTotalPlans.setText(String.valueOf(totalPlans));

                binding.etRemainingPlans.setText(String.valueOf(remainingPlans));
            }
        });
        binding.etTotalPlans.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().equals("")){
                    binding.etApprovedPlans.setText("");
                    binding.etRemainingPlans.setText("");
                }
            }
        });
        binding.etApprovedPlans.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String approvedPlans = s.toString();
                int approvedPlanInteger = 0;
                int totalPlanInteger = 0;

                if(approvedPlans.equals("")) {
//                    if nothing approved then total are remaining plans to be approved
                    binding.etRemainingPlans.setText(binding.etTotalPlans.getText().toString());
                }else{
                    approvedPlanInteger = Integer.parseInt(approvedPlans);

                    if(!binding.etTotalPlans.getText().toString().equals(""))
                        totalPlanInteger = Integer.parseInt(binding.etTotalPlans.getText().toString());

                    if(approvedPlanInteger>totalPlanInteger) {
                        binding.etApprovedPlans.setText("");
                        approvedPlanInteger=0;
                        Toast.makeText(context, "Approved plans cannot be greater than total plans.", Toast.LENGTH_SHORT).show();
                    }

                }
                binding.etRemainingPlans.setText((String.valueOf(totalPlanInteger - approvedPlanInteger)));
            }
        });
    }

    private void checkingPreviousData() {

        if(formDataObject.count(FormData.class) > 0) {

            formDataObject = new FormData();
            FormData formDataObjectTemp = FormData.last(FormData.class);
            formDataObject.setPendingBuilding(formDataObjectTemp.getRemainingBuilding());
            formDataObject.setTotalBuilding(formDataObjectTemp.getRemainingBuilding());
            formDataObject.setRemainingBuilding(formDataObjectTemp.getRemainingBuilding());
        }else{
            formDataObject = new FormData();
        }

        binding.tvDateAdded.setText(df.format(calobj.getTime()));
        binding.tvDateApproved.setText(df.format(calobj.getTime()));

//        if(!Globals.getUsage().sharedPreferencesEditor.firstTimeLogin()){
        if(userData.getRecordExist()==1){
            binding.etPendingPlans.setEnabled(false);
        }
    }

    public FormData getModel() {
        return formDataObject;
    }

    public void setModel(FormData model) {
        this.formDataObject = model;
    }

    public void openHomeActivityAgain(){
        Intent intent = new Intent(activity, DrawerActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
        activity.finish();
    }
}
