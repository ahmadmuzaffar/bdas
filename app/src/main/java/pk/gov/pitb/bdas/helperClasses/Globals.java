package pk.gov.pitb.bdas.helperClasses;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.google.gson.JsonObject;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;
import pk.gov.pitb.bdas.R;
import pk.gov.pitb.bdas.dialogues.SweetAlertDialogs;
import pk.gov.pitb.bdas.sharedPreference.SharedPreferencesEditor;

public class Globals {

	private static Globals instance = null;

	public static Globals getUsage() {
		return instance;
	}

	public static Globals getInstance(Activity activity) {
		if (instance == null) {
			instance = new Globals(activity);
		}
		instance.setActivityOnFront(activity);
		return instance;
	}

	public Locale mLocale;
	public Activity mActivity;
	public Context mContext;
	public String mPackageName;
//	public String mImeiNumber;
	public int mScreenWidth;
	public int mScreenHeight;
	public SharedPreferencesEditor sharedPreferencesEditor;
	public static Typeface typeface = null;
	public DisplayImageOptions options;


	private Globals(Activity activity) {
		mLocale = Locale.ENGLISH;
		mActivity = activity;
		mContext = activity;
		mPackageName = mContext.getPackageName();
		sharedPreferencesEditor = new SharedPreferencesEditor(mContext);
//		if(!checkPermission(Manifest.permission.READ_PHONE_STATE))
//			requestPermission(new String[]{Manifest.permission.READ_PHONE_STATE},2);
//		TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
//		mImeiNumber = telephonyManager.getDeviceId();
//		mImeiNumber = "864390020196622";

		Point size = new Point();
		mActivity.getWindowManager().getDefaultDisplay().getSize(size);
		mScreenWidth = size.x;
		mScreenHeight = size.y;
//		setUrduFont();

		options = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.logo)
				.showImageForEmptyUri(R.drawable.logo)
				.showImageOnFail(R.drawable.logo)
				.cacheInMemory(true)
				.cacheOnDisk(true)
				.considerExifParams(true)
				.displayer(new SimpleBitmapDisplayer())
				.imageScaleType(ImageScaleType.EXACTLY)
				.resetViewBeforeLoading(true)
				.build();

	}

	public void resetInstance() {
		instance = null;
	}

	private void setActivityOnFront(Activity activity) {
		mActivity = activity;
		mContext = activity;
	}

	public boolean isInternetAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
	public static String getDateTime() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.FORMAT_DATE_TIME, Locale.ENGLISH);
		String value = sdf.format(date);
		return value;
	}
	public static String getDate() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.FORMAT_DATE_APP, Locale.ENGLISH);
		String value = sdf.format(date);
		return value;
	}
	public static String getTime() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.FORMAT_TIME_APP, Locale.ENGLISH);
		String value = sdf.format(date);
		return value;
	}
	public static String changeDateFormat(String dateString){
		try {
			// parse the String "29/07/2013" to a java.util.Date object
			Date date = null;
			date = new SimpleDateFormat(Constants.FORMAT_DATE_TIME).parse(dateString);
			// format the java.util.Date object to the desired format
			String formattedDate = new SimpleDateFormat(Constants.VIEW_FORMAT_DATE_TIME).format(date);
		return formattedDate;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dateString;
	}
	public boolean checkPermission(String permission){
		int result = ContextCompat.checkSelfPermission(mContext, permission);
		if (result == PackageManager.PERMISSION_GRANTED){

			return true;

		} else {

			return false;

		}
	}
	public static void closeKeyBoard(Context context) {
		try {
			InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
			inputMethodManager.hideSoftInputFromWindow(((Activity) context).getCurrentFocus().getWindowToken(), 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void requestPermission(String[] permissions, int RQ_code){

		if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.ACCESS_FINE_LOCATION)){

//			Toast.makeText(mContext,"GPS permission allows us to access location data. Please allow in App Settings for additional functionality.",Toast.LENGTH_LONG).show();

		} else {

			ActivityCompat.requestPermissions(mActivity, permissions, RQ_code);
		}
	}
	public static void setUrduFont() {
		try {
			if (Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD_MR1) {
				typeface = Typeface.createFromAsset(Globals.getUsage().mContext.getAssets(), "fonts/jameel_noori_nastaleeq.ttf");
			} else {
				typeface = Typeface.createFromAsset(Globals.getUsage().mContext.getAssets(), "fonts/nastaleeq_like.ttf");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static int getAppVersion(){
		PackageInfo pInfo = null;
		try {
			pInfo = Globals.getUsage().mContext.getPackageManager().getPackageInfo(Globals.getUsage().mContext.getPackageName(), 0);
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}
		return pInfo.versionCode;
	}

	//	public JsonObject getJSON(String locationString) {
	public JsonObject getJSON() {
		JsonObject jsonObject = new JsonObject();
		try {

//			jsonObject.addProperty("time", getTime());
//			jsonObject.addProperty("date", getDate());
			jsonObject.addProperty("activity_datetime", getDateTime());
//			jsonObject.addProperty("imei_number", Globals.getUsage().mImeiNumber);
//			jsonObject.addProperty("user_id", Globals.getUsage().sharedPreferencesEditor.getUserID());
			jsonObject.addProperty("app_version", getAppVersion());
//			jsonObject.addProperty("location", locationString);
//			jsonObject.addProperty("network_time", getTime());
//			jsonObject.addProperty("network_date", getDate());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObject;
	}

	public static boolean isEmailValid(String email) {
		String expression = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}

	public static void hideKeyboard(Activity activity) {
		View view = activity.findViewById(android.R.id.content);
		if (view != null) {
			InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		}
	}

	public static String getAppVersionName(){
		PackageInfo pInfo = null;
		try {
			pInfo = Globals.getUsage().mContext.getPackageManager().getPackageInfo(Globals.getUsage().mContext.getPackageName(), 0);
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}
		return pInfo.versionName;
	}
	public static int getAppVersionCode(){
		PackageInfo pInfo = null;
		try {
			pInfo = Globals.getUsage().mContext.getPackageManager().getPackageInfo(Globals.getUsage().mContext.getPackageName(), 0);
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}
		return pInfo.versionCode;
	}
	public void showDialogFailure(String message){
		SweetAlertDialogs.getInstance().showDialogOK(Globals.getUsage().mContext, "Failure", message, new SweetAlertDialogs.OnDialogClickListener() {
			@Override
			public void onClick(SweetAlertDialog sweetAlertDialog) {
				sweetAlertDialog.dismiss();
//				Globals.getUsage().mActivity.finish();
			}
		}, false);
	}

}