package pk.gov.pitb.bdas.fragments.login;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pk.gov.pitb.bdas.R;
import pk.gov.pitb.bdas.StartActivity;
import pk.gov.pitb.bdas.databinding.FragmentLoginBinding;

public class LoginFragment extends Fragment {

    FragmentLoginBinding binding;
    LoginViewModel loginViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);
        loginViewModel = new LoginViewModel(getContext(), (StartActivity) getActivity(), binding);
        binding.setLoginViewModel(loginViewModel);
        binding.setLifecycleOwner(this);

        return binding.getRoot();
    }
}