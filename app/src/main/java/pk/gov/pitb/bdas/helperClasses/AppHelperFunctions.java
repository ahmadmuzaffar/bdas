package pk.gov.pitb.bdas.helperClasses;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.ClipboardManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import pk.gov.pitb.bdas.R;

import static android.content.Context.CLIPBOARD_SERVICE;

/**
 * Created by PITB on 1/18/2016.
 */
public class AppHelperFunctions {
///////////////////////////////              input screens UI function              /////////////////////////////////////////////////

    public static int layoutRowPadding = (int) (Globals.getUsage().mScreenWidth * .02);

    public static void setScrollViewParams(ScrollView scrollView){
        LinearLayout.LayoutParams paramsscrollviewLayout = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        int margin = (int) (Globals.getUsage().mScreenWidth * .03);
        paramsscrollviewLayout.setMargins(0,margin,0,margin);
        scrollView.setLayoutParams(paramsscrollviewLayout);
    }
    // input screens UI function
    public static void createButton(Button button, String text, int bgColorCodeOn, int bgColorCodeOff, int textColorCode, Drawable drawableOn,
                                    Drawable drawableOff, String fontName, float fontSize, int width, int height, int[] margins) {
        LinearLayout.LayoutParams paramsButton = new LinearLayout.LayoutParams(width, height);
        paramsButton.setMargins(margins[0], margins[1], margins[2], margins[3]);
        button.setLayoutParams(paramsButton);
        button.setPadding(0, 0, 0, 0);
        button.setGravity(Gravity.CENTER);
        button.setBackgroundColor(bgColorCodeOn);
        button.setTextColor(textColorCode);
        button.setText(text);
        button.setTextSize(18);

//        if(drawableOn!=null) {
//            StateListDrawable statesButton1 = new StateListDrawable();
//            statesButton1.addState(new int[]{android.R.attr.state_pressed}, drawableOff);
//            statesButton1.addState(new int[]{}, drawableOn);
//            button.setBackgroundDrawable(statesButton1);
//        }
    }

    // input screens UI function
    public static void addHeading(String label, LinearLayout linearLayout, Drawable drawable) {
        TextView textViewLabel = new TextView(Globals.getUsage().mContext);
        LinearLayout.LayoutParams paramsLayoutRow = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                (int) (Globals.getUsage().mScreenWidth * .12));
        paramsLayoutRow.setMargins(0, (int) (Globals.getUsage().mScreenWidth * .02), 0, (int) (Globals.getUsage().mScreenWidth * .01));
        textViewLabel.setLayoutParams(paramsLayoutRow);
        textViewLabel.setTypeface(null, Typeface.BOLD);
        textViewLabel.setText(label);
        textViewLabel.setTypeface(null, Typeface.BOLD);
        textViewLabel.setTextColor(Color.BLACK);
        textViewLabel.setPadding((int) (Globals.getUsage().mScreenWidth * .02), 0, 0, 0);
        textViewLabel.setGravity(Gravity.CENTER);
        linearLayout.addView(textViewLabel);
        if(drawable!=null)
            textViewLabel.setBackgroundDrawable(drawable);
    }
    public static void setViewParams(View view){
        view.setBackgroundColor(Globals.getUsage().mContext.getResources().getColor(R.color.colorBlack));
        LinearLayout.LayoutParams paramsView = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                (int) (Globals.getUsage().mScreenWidth * .003));
        int margin = (int) (Globals.getUsage().mScreenWidth * .02);
//        paramsView.setMargins(margin,0,margin,0);
        view.setLayoutParams(paramsView);
//    view.setPadding((int) (Globals.getUsage().mScreenWidth * .2),0,(int) (Globals.getUsage().mScreenWidth * .2),0);
    }



/////////////////////////////////////////////////////////////////////////////////////////////////////////



    /////////////////////////////       Detail Page UI Functions ////////////////////////////////////////////
    public static void showDialogAction(final String msg) {
        try {
            String[] actions = new String[] { "Copy to clipboard" };

            new AlertDialog.Builder(Globals.getUsage().mContext).setTitle(msg).setItems(actions, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    try {
                        ClipboardManager clipboard = (ClipboardManager) Globals.getUsage().mContext.getSystemService(CLIPBOARD_SERVICE);

                        clipboard.setText(msg);
                        Toast.makeText(Globals.getUsage().mContext, "Copied to clipboard", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).setCancelable(true).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setTextViewDetail(TextView textView){
        LinearLayout.LayoutParams paramstextviewValue = null;
        paramstextviewValue = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        textView.setLayoutParams(paramstextviewValue);
        int paddingRow = (int) (Globals.getUsage().mScreenWidth * .02);
        int paddingRowTop = (int) (Globals.getUsage().mScreenWidth * .03);
//        int paddingRowVertical = (int) (Globals.getUsage().mScreenWidth * .03);
        textView.setPadding(paddingRow,paddingRowTop,paddingRow,0);
//        textView.setSingleLine(false);
//        textView.setTypeface(null, Typeface.NORMAL);
        textView.setTextColor(Color.GRAY);
        textView.setBackgroundColor(Color.TRANSPARENT);
//        textView.setEnabled(true);
//        textView.setFocusable(false);
        textView.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
    }


    public static void setHeadingLayoutRow(LinearLayout layoutRow){
        LinearLayout.LayoutParams paramsLayoutRow = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                (int) (Globals.getUsage().mScreenWidth * .12));
        paramsLayoutRow.setMargins(0, (int) (Globals.getUsage().mScreenWidth * .03), 0, 0);
        layoutRow.setLayoutParams(paramsLayoutRow);
        layoutRow.setOrientation(LinearLayout.HORIZONTAL);
        layoutRow.setGravity(Gravity.CENTER_VERTICAL);
        int padding = (int) (Globals.getUsage().mScreenWidth * .02);
        layoutRow.setPadding(0,padding,0,padding);

    }
    public static void setLabelTextView(TextView textViewLabel, String label){
        textViewLabel.setTypeface(null, Typeface.BOLD);
        LinearLayout.LayoutParams paramstextviewLabel = new LinearLayout.LayoutParams((int) (Globals.getUsage().mScreenWidth * .45),
                LinearLayout.LayoutParams.MATCH_PARENT);
        textViewLabel.setLayoutParams(paramstextviewLabel);
        textViewLabel.setText(label);
        textViewLabel.setTypeface(null, Typeface.BOLD);
        textViewLabel.setTextColor(Color.WHITE);
//		textViewLabel.setBackgroundColor(Color.parseColor(headingRowColor));
//		textViewLabel.setBackgroundResource(R.color.heading_blue);
        textViewLabel.setTextSize(18);
        //textViewLabel.setBackgroundDrawable(drawable);
        textViewLabel.setPadding((int) (Globals.getUsage().mScreenWidth * .02), 0, 0, 0);
        textViewLabel.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);

    }

    public static void addDataBlockHeading(String title, int listCount, LinearLayout scrollviewLayout) {
        LinearLayout layout = new LinearLayout(Globals.getUsage().mContext);
        LinearLayout.LayoutParams paramsLayout = new LinearLayout.LayoutParams((int) (Globals.getUsage().mScreenWidth * .90),
                LinearLayout.LayoutParams.WRAP_CONTENT);
        paramsLayout.setMargins(0, (int) (Globals.getUsage().mScreenWidth * .03), 0, 0);

        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setLayoutParams(paramsLayout);
        layout.setGravity(Gravity.CENTER_HORIZONTAL);
        layout.setBackgroundColor(Globals.getUsage().mContext.getResources().getColor(R.color.colorBlack));
//        Drawable drawable = Globals.mContext.getResources().getDrawable(R.drawable.header_input);
//        drawable = Globals.mContext.getResources().getDrawable(R.drawable.header_search);
//        addHeading(title, listCount, layout, drawable);
        scrollviewLayout.addView(layout);
    }

    public static void addHeading(String label, LinearLayout layout, Drawable drawableRight, int color) {
        LinearLayout layoutRow = new LinearLayout(Globals.getUsage().mContext);
        TextView textViewLabel = new TextView(Globals.getUsage().mContext);
        setHeadingLayoutRow(layoutRow);
        setLabelTextView(textViewLabel,label);
        LinearLayout.LayoutParams paramstextviewLabel = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        textViewLabel.setLayoutParams(paramstextviewLabel);
        textViewLabel.setCompoundDrawables(null,null,drawableRight,null);
        textViewLabel.setTextColor(color);
        layoutRow.addView(textViewLabel);
        layout.addView(layoutRow);

    }
    public static void addHeading(String label, int listCount, LinearLayout layout, Drawable drawable) {
        LinearLayout layoutRow = new LinearLayout(Globals.getUsage().mContext);
        TextView textViewLabel = new TextView(Globals.getUsage().mContext);
        TextView textViewLabel2 = new TextView(Globals.getUsage().mContext);

        setHeadingLayoutRow(layoutRow);
        setLabelTextView(textViewLabel, label);
        setLabelTextView(textViewLabel2,listCount+"");

        layoutRow.addView(textViewLabel);
        layoutRow.addView(textViewLabel2);
        layout.addView(layoutRow);

    }
    //
//
//    public static String setDataString(List<ClassTitleValue> titleValueList, String appender){
//        String data = "";
//        for (int i = 0; i < titleValueList.size(); i++)
//            if(titleValueList.get(i).value.length()>0)
//                data += titleValueList.get(i).title + " " + titleValueList.get(i).value + "\n";
//        return data;
//    }
    public static String addSeperator() {
        String seperator = "--------------------\n";
        return seperator;
    }

    // input screens UI function
    public static void showDialogExit() {
        try {
            new AlertDialog.Builder(Globals.getUsage().mContext).setMessage("Are you sure you want to exit?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Globals.getUsage().mActivity.finish();
                }
            }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).show();
        } catch (Exception e) {
            Log.e("Exception", e.getMessage());
        }
    }


}
