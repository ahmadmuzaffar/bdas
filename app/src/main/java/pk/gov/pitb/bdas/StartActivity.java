package pk.gov.pitb.bdas;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import pk.gov.pitb.bdas.fragments.login.LoginFragment;
import pk.gov.pitb.bdas.fragments.signup.SignupFragment;
import pk.gov.pitb.bdas.helperClasses.CallBackFragment;

public class StartActivity extends AppCompatActivity implements CallBackFragment {

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    SignupFragment signupFragment = null;
    LoginFragment loginFragment = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        addFragment();

    }

    public void addFragment() {
        if(loginFragment == null)
            loginFragment = new LoginFragment();
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragmentContainer, loginFragment);
        fragmentTransaction.commit();
    }

    public void replaceFragment(String username){
        if(signupFragment == null)
            signupFragment = new SignupFragment();
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.replace(R.id.fragmentContainer, signupFragment);
        signupFragment.setUsername(username);
        fragmentTransaction.commit();
    }

    @Override
    public void changeFragment(String username) {
        replaceFragment(username);
    }

}
