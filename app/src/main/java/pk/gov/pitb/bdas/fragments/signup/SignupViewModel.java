package pk.gov.pitb.bdas.fragments.signup;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.jaeger.library.StatusBarUtil;
import com.orm.SugarContext;

import butterknife.ButterKnife;
import pk.gov.pitb.bdas.SplashActivity;
import pk.gov.pitb.bdas.StartActivity;
import pk.gov.pitb.bdas.databinding.FragmentSignupBinding;
import pk.gov.pitb.bdas.helperClasses.Globals;
import pk.gov.pitb.bdas.model.loginResponse.UserData;
import pk.gov.pitb.bdas.serverRetrofit.APIService;
import pk.gov.pitb.bdas.serverRetrofit.ApiUtils;

public class SignupViewModel extends BaseObservable {

    private FragmentSignupBinding binding;
    private UserData user;
    //private GoogleApiClient googleApiClient;
    private ProgressDialog progressDialog;
    private Globals mGlobals;
    private APIService mAPIService;
    String recaptchaToken = "";

    @SuppressLint("StaticFieldLeak")
    private Context mContext;

    @SuppressLint("StaticFieldLeak")
    private StartActivity activity;

    public SignupViewModel(Context context, StartActivity activity, FragmentSignupBinding binding) {

        this.binding = binding;
        this.mContext = context;
        this.activity = activity;
        user = new UserData();
        setInstances();
        listeners();

    }

    private void setInstances(){
        mGlobals = Globals.getInstance(activity);
        SugarContext.init(mContext);
        ButterKnife.bind(activity);
        mAPIService = ApiUtils.getAPIService();
        StatusBarUtil.setTransparent(activity);

    }

    @Bindable
    public UserData getUser() {
        return user;
    }

    public void setUser(UserData user) {
        this.user = user;
//        notifyPropertyChanged(pk.gov.pitb.bdas.BR.signupViewModel);
    }

    private void listeners() {

//        binding.etCnic.addTextChangedListener(new TextWatcherCNIC(binding.etCnic));
//        binding.etCPassword.addTextChangedListener(new TextWatcherPassword(binding.etCPassword));
//        binding.etPhone.addTextChangedListener(new TextWatcherMobileNo(binding.etPhone));
//        binding.etPassword.addTextChangedListener(new TextWatcherPassword(binding.etPassword));

//        googleApiClient = new GoogleApiClient.Builder(this)
//                .addApi(SafetyNet.API)
//                .addConnectionCallbacks(NewSignUpActivity.this)
//                .build();
//        googleApiClient.connect();
//
//        captcha.setOnCheckedChangeListener((buttonView, isChecked) -> {
//            captcha.setError(null);
//        });
//
//        captcha.setOnClickListener(v -> {
//            if(captcha.isChecked()){
//                SafetyNet.SafetyNetApi.verifyWithRecaptcha(googleApiClient, siteKey())
//                        .setResultCallback(new ResultCallback<SafetyNetApi.RecaptchaTokenResult>() {
//                            @Override
//                            public void onResult(@NonNull SafetyNetApi.RecaptchaTokenResult recaptchaTokenResult) {
//                                if(recaptchaTokenResult.getStatus().isSuccess()){
//                                    Toast.makeText(getApplicationContext(), "Success - کامیابی", Toast.LENGTH_LONG).show();
//                                    String userResponseToken = recaptchaTokenResult.getTokenResult();
//                                    if (!userResponseToken.isEmpty()) {
//                                        //Server Handling
//                                    }
//                                }
//                                else{
//                                    Toast.makeText(getApplicationContext(), "Failure - ناکامی", Toast.LENGTH_LONG).show();
//                                    captcha.setChecked(false);
//                                }
//                            }
//                        });

//        binding.btnSignup.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(validateFields()){
//                    signUp();
//                }
//            }
//        });

    }

    private void signUp() {

        progressDialog = new ProgressDialog(mContext);
        progressDialog.setMessage("Signing up... | سائن اپ");
        progressDialog.setCancelable(false);
        progressDialog.show();

//        JsonObject jsonObject = new JsonObject();
//        try {
//            jsonObject.addProperty("fullname", binding.etFullname.getText().toString());
//            jsonObject.addProperty("cnic", binding.etCnic.getText().toString());
//            jsonObject.addProperty("email", binding.etEmail.getText().toString());
//            jsonObject.addProperty("contact_number", binding.etPhone.getText().toString());
//            jsonObject.addProperty("password", binding.etPassword.getText().toString());
//            jsonObject.addProperty("password_confirmation", binding.etCPassword.getText().toString());
//            jsonObject.addProperty("token", recaptchaToken);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        Map<String, String> hashMap = new HashMap();
//        hashMap.put("data", jsonObject.toString());
//        hashMap.put("app_data", Globals.getUsage().getJSON().toString());
//
//
//        mAPIService.signup(hashMap).enqueue(new Callback<LoginResponse>() {
//            @Override
//            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
//                progressDialog.dismiss();
//                if (response.isSuccessful()) {
//                    Log.i("TAG", "post submitted to API." + response.body().toString());
//                    LoginResponse loginResponse = response.body();
//                    if(loginResponse.getStatus()){
//                        // login success
//
//                        UserData userData = loginResponse.getUserData();
//                        if(userData!=null) {
//                            userData.save();
//                            openSplashActivity();
//                        }else{
//                            SweetAlertDialogs.getInstance().showDialogOK(mGlobals.mContext, "Error - خرابی", "User Data is not there in response\nجواب میں صارف کا ڈیٹا موجود نہیں ہے", null, false);
//                        }
//                        Globals.getUsage().sharedPreferencesEditor.setLoggedIn(true);
//
//                    }else{
//                        SweetAlertDialogs.getInstance().showDialogOK(mGlobals.mContext, activity.getString(R.string.cannot_signup), loginResponse.getMessage(), null, false);
//                    }
//                } else {
//                    Globals.getUsage().showDialogFailure("Please contact admin - ایڈمن سے رابطہ کریں");
//                }
//            }
//
//            @Override
//            public void onFailure(Call<LoginResponse> call, Throwable t) {
//                progressDialog.dismiss();
//                Log.e("failure", "Unable to submit post to API.");
//                if (t instanceof IOException) {
//                    Globals.getUsage().showDialogFailure(Globals.getUsage().mContext.getString(R.string.network_error));     // logging probably not necessary
//                }
//                else {
//                    Globals.getUsage().showDialogFailure(Globals.getUsage().mContext.getString(R.string.error_message));
//                }
//            }
//        });


    }

    private void openSplashActivity() {
        Intent intent = new Intent(activity, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
        activity.finish();
    }

//    private boolean validateFields() {
//
//        if(binding.etFullname.length() == 0){
//            binding.etFullname.setError("Enter Full Name");
//            return false;
//        }
//        if(binding.etFullname.length() > 20){
//            binding.etFullname.setError("Maximum 20 characters are allowed");
//            return false;
//        }
//        if(binding.etCnic.length() == 0){
//            binding.etCnic.setError("Enter Cnic");
//            return false;
//        }
//        if(binding.etEmail.length() == 0){
//            binding.etEmail.setError("Enter Email Address");
//            return false;
//        }
//        if(binding.etPhone.length() == 0){
//            binding.etPhone.setError("Enter Phone Number");
//            return false;
//        }if(binding.etPassword.length() == 0){
//            binding.etPassword.setError("Enter Password");
//            return false;
//        }
//        if(binding.etPassword.length() < 6){
//            binding.etPassword.setError("Minimum 6 characters required");
//            return false;
//        }
//        if(binding.etCPassword.length() == 0){
//            binding.etCPassword.setError("Re-Enter Password");
//            return false;
//        }
//        if(binding.etCPassword.length() < 6){
//            binding.etCPassword.setError("Minimum 6 characters required");
//            return false;
//        }
//        if(!binding.etCPassword.getText().toString().equals(binding.etPassword.getText().toString())){
//            binding.etCPassword.setError("Passwords did not match");
//            return false;
//        }
//        return true;
//
//    }
}