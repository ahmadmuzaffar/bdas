package pk.gov.pitb.bdas;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.jaeger.library.StatusBarUtil;
import com.orm.SugarContext;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import pk.gov.pitb.bdas.dialogues.SweetAlertDialogs;
import pk.gov.pitb.bdas.fragments.home.HomeFragment;
import pk.gov.pitb.bdas.fragments.unsent.UnsentFragment;
import pk.gov.pitb.bdas.helperClasses.DatabaseHelper;
import pk.gov.pitb.bdas.helperClasses.Globals;
import pk.gov.pitb.bdas.model.loginResponse.FormData;
import pk.gov.pitb.bdas.model.loginResponse.UserData;
import pk.gov.pitb.bdas.serverRetrofit.APIService;
import pk.gov.pitb.bdas.serverRetrofit.ApiUtils;

public class DrawerActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    @BindView(R.id.tv_title)
    public AppCompatTextView mTextView;

    NavigationView navigationView;
    ActionBarDrawerToggle mDrawerToggle;
    DrawerLayout drawer;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private HomeFragment homeFragment = null;
    private UnsentFragment unsentFragment = null;
    private APIService mAPIService;

    @Override
    protected void onResume() {
        setInstances();
        setUserInfo();
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);
        setInstances();
        initializeActivity();
    }

    private void initializeActivity() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        mDrawerToggle  = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        navigationView.setCheckedItem(R.id.nav_home);
        openHomeFragment();
        setUserInfo();
    }

    private  void setInstances(){
        Globals.getInstance(this);
        ButterKnife.bind(this);
        SugarContext.init(this);
        mAPIService = ApiUtils.getAPIService();
        StatusBarUtil.setTransparent(this);
    }

    @SuppressLint("SetTextI18n")
    public void setUserInfo(){

        UserData userData = UserData.last(UserData.class);

        View headerView = navigationView.getHeaderView(0);
        TextView navUsername =  headerView.findViewById(R.id.nav_username);
        TextView navEmail = headerView.findViewById(R.id.nav_email);

        navUsername.setText(userData.getUserName());
        navEmail.setText(userData.getEmail());
//            if(classFarmer.getPicturePath() != null) {
//                try {
//                    ImageLoader.getInstance().displayImage(classFarmer.getPicturePath(), imageView, options, null);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }

//            if (classFarmer.getPicturePath()!=null && !classFarmer.getPicturePath().equals("")
//                    && classFarmer.getPicturePath().contains("http")) {
//                try{
//                    ImageLoader.getInstance().displayImage(classFarmer.getPicturePath(), imageView, options, null);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }else{
//                if (classFarmer.getPictureURI()!=null && !classFarmer.getPictureURI().equals("")) {
//                    try {
//                        ImageLoader.getInstance().displayImage(classFarmer.getPictureURI(), imageView, options, null);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else{
            Fragment currentFragment = fragmentManager.findFragmentById(R.id.nav_host_fragment);
            if (currentFragment instanceof HomeFragment) {
                alertViewExit();
            }else{
                fragmentManager.popBackStack();
                mTextView.setText("Home");
                navigationView.setCheckedItem(R.id.nav_home);
            }

        }
    }

    private void alertViewExit() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Exit?")
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        dialoginterface.cancel();
                    }})
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        dialoginterface.dismiss();
                        finish();
                    }
                }).show();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        Fragment currentFragment = fragmentManager.findFragmentById(R.id.nav_host_fragment);

        if (id == R.id.nav_home) {
            if(!(currentFragment instanceof HomeFragment)) {
                fragmentManager.popBackStack();
                drawer.closeDrawer(GravityCompat.START);
                mTextView.setText("Home");
                return true;
            }
        } else if (id == R.id.nav_unsent) {
            if(!(currentFragment instanceof UnsentFragment))
                openUnsentFragment();
//        }else if(id == R.id.nav_sync){
//            sync();
        }else if (id == R.id.nav_logout) {
            logout();
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

//    private void sync() {
//        Globals.getUsage().sharedPreferencesEditor.setSyncRequired(true);
//        DatabaseHelper.resetDatabaseSyncData();
//        Intent intent = new Intent(this, SplashActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(intent);
//        finish();
//    }

    private void openUnsentFragment() {
        if(FormData.count(FormData.class, "is_unsent = ?", new String[]{"1"})>0) {
            mTextView.setText("Unsent");
            if (unsentFragment == null)
                unsentFragment = new UnsentFragment();
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.nav_host_fragment, unsentFragment, "Unsent");
            fragmentTransaction.addToBackStack(unsentFragment.getClass().getName());
            fragmentTransaction.commit();
        }else{
            Toast.makeText(this, "No unsent available", Toast.LENGTH_SHORT).show();
        }
    }

    private void logout() {
        SweetAlertDialogs.getInstance().showDialogYesNo(this, "Logout", "Are you sure you want to logout?", new SweetAlertDialogs.OnDialogClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        Globals.getUsage().sharedPreferencesEditor.clearPreferences();
                        DatabaseHelper.resetAllDatabase();
                        Intent intent = new Intent(DrawerActivity.this, SplashActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }
                },
                new SweetAlertDialogs.OnDialogClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                }, false);
    }

    private void openHomeFragment() {
        mTextView.setText("Home");
        if(homeFragment == null)
            homeFragment = new HomeFragment();
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.nav_host_fragment, homeFragment, "Home");
        fragmentTransaction.commit();
    }
}
