package pk.gov.pitb.bdas.fragments.unsent;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pk.gov.pitb.bdas.R;
import pk.gov.pitb.bdas.databinding.FragmentUnsentBinding;

public class UnsentFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        FragmentUnsentBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_unsent, container, false);
        UnsentViewModel unsentViewModel = new UnsentViewModel(getActivity(), getContext(), binding);
        binding.setUnsentViewModel(unsentViewModel);
        binding.setLifecycleOwner(this);

        return binding.getRoot();
    }
}
