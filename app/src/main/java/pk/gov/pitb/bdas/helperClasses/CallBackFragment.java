package pk.gov.pitb.bdas.helperClasses;

public interface CallBackFragment {
    void changeFragment(String username);
}
