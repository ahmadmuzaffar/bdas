package pk.gov.pitb.bdas.textWatchers;

import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.widget.EditText;

public class TextWatcherUsername implements TextWatcher {
    private EditText editText = null;

    public TextWatcherUsername(EditText editText) {
        this.editText = editText;
        this.editText.setFilters(new InputFilter[] { new InputFilter.LengthFilter(15) });
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
        try {
            if (s.length() > 0 && s.length() <= 10) {
                editText.setError(null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void afterTextChanged(Editable s) {
    }
}
