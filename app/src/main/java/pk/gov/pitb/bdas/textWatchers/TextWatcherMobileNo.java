package pk.gov.pitb.bdas.textWatchers;

import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.widget.EditText;

public class TextWatcherMobileNo implements TextWatcher {

	private EditText editText = null;

	public TextWatcherMobileNo(EditText editText) {
		this.editText = editText;
		this.editText.setInputType(InputType.TYPE_CLASS_PHONE);
//		this.editText.setTransformationMethod(PasswordTransformationMethod.getInstance());
		this.editText.setFilters(new InputFilter[] { new InputFilter.LengthFilter(11) });
		this.editText.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
	}

	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	}

	public void onTextChanged(CharSequence s, int start, int before, int count) {
		try {
			String text = editText.getText().toString();
			if (text.length() == 1 && !text.equals("0")) {
				editText.setText("");
			} else if (text.length() > 1 && !text.startsWith("0")) {
				int length = editText.getText().toString().length();
				editText.setText(text.substring(1, length));
				editText.setSelection(length);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void afterTextChanged(Editable s) {
	}
}