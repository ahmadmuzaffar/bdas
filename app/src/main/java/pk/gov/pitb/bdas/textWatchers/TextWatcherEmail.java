package pk.gov.pitb.bdas.textWatchers;

import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.widget.EditText;

import pk.gov.pitb.bdas.helperClasses.Globals;

public class TextWatcherEmail implements TextWatcher {

    private EditText editText = null;

    public TextWatcherEmail(EditText editText) {
        this.editText = editText;
		this.editText.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        this.editText.setFilters(new InputFilter[] { new InputFilter.LengthFilter(30) });
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if(s.length() != 0 && s.length() <= 30){
            if(Globals.isEmailValid(s.toString())){
                editText.setError(null);
            }
        }
    }
}
