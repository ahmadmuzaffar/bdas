package pk.gov.pitb.bdas.helperClasses;

import pk.gov.pitb.bdas.model.loginResponse.FormData;
import pk.gov.pitb.bdas.model.loginResponse.UserData;

public class DatabaseHelper {

    public static void resetAllDatabase(){
        try{
            UserData.deleteAll(UserData.class);
            FormData.deleteAll(FormData.class);
        }catch(Exception e){
            e.printStackTrace();
        }
    }



}
