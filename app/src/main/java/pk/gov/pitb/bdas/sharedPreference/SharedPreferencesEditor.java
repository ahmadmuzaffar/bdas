package pk.gov.pitb.bdas.sharedPreference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SharedPreferencesEditor {


	private Context mContext = null;

	/* APPLICATION CONSTANTS */
	private final String PREF_FILE_APP = "ApplicationDetails";

	private final String PREF_LOGGED_IN = "loggedIn";
//	private final String PREF_FIRST_TIME_LOGIN = "firstTimeLogin";

	public static SharedPreferences getSharedPreferences(Context ctx)
	{
		return PreferenceManager.getDefaultSharedPreferences(ctx);
	}

	public SharedPreferencesEditor(Context context) {
		this.mContext = context;
	}

	public void clearPreferences(){
		SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_APP, Context.MODE_PRIVATE).edit();
		editor.clear().commit();

	}

	public boolean IsLoggedIn() {
		try {
			SharedPreferences sharedPreferences = mContext.getSharedPreferences(PREF_FILE_APP, Context.MODE_PRIVATE);
			boolean value = sharedPreferences.getBoolean(PREF_LOGGED_IN, false);
			return value;
		} catch (Exception e) {
			e.printStackTrace();
			return true;
		}
	}

	public boolean setLoggedIn(boolean syncRequired) {
		try {
			SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_APP, Context.MODE_PRIVATE).edit();
			editor.putBoolean(PREF_LOGGED_IN, syncRequired);
			return editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

//	public boolean firstTimeLogin() {
//		try {
//			SharedPreferences sharedPreferences = mContext.getSharedPreferences(PREF_FILE_APP, Context.MODE_PRIVATE);
//			boolean value = sharedPreferences.getBoolean(PREF_FIRST_TIME_LOGIN, false);
//			return value;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return true;
//		}
//	}
//
//	public boolean setFirstTimeLogin(boolean firstTimeLogin) {
//		try {
//			SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_FILE_APP, Context.MODE_PRIVATE).edit();
//			editor.putBoolean(PREF_FIRST_TIME_LOGIN, firstTimeLogin);
//			return editor.commit();
//		} catch (Exception e) {
//			e.printStackTrace();
//			return false;
//		}
//	}
}