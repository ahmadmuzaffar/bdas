package pk.gov.pitb.bdas.fragments.signup;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pk.gov.pitb.bdas.R;
import pk.gov.pitb.bdas.StartActivity;
import pk.gov.pitb.bdas.databinding.FragmentSignupBinding;
import pk.gov.pitb.bdas.model.loginResponse.UserData;

public class SignupFragment extends Fragment {

    public String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String name) {
        username = name;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        FragmentSignupBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_signup, container, false);
        SignupViewModel signupViewModel= new SignupViewModel(getContext(), (StartActivity) getActivity(), binding);
        binding.setSignupViewModel(signupViewModel);
        binding.setLifecycleOwner(this);
        UserData user = new UserData();
        user.setUserName(getUsername());
        signupViewModel.setUser(user);

        return binding.getRoot();
    }
}