package pk.gov.pitb.bdas.fragments.login;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.google.gson.JsonObject;
import com.jaeger.library.StatusBarUtil;
import com.orm.SugarContext;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import pk.gov.pitb.bdas.DrawerActivity;
import pk.gov.pitb.bdas.R;
import pk.gov.pitb.bdas.StartActivity;
import pk.gov.pitb.bdas.databinding.FragmentLoginBinding;
import pk.gov.pitb.bdas.dialogues.SweetAlertDialogs;
import pk.gov.pitb.bdas.helperClasses.CallBackFragment;
import pk.gov.pitb.bdas.helperClasses.DatabaseHelper;
import pk.gov.pitb.bdas.helperClasses.Globals;
import pk.gov.pitb.bdas.model.loginResponse.FormData;
import pk.gov.pitb.bdas.model.loginResponse.LoginResponse;
import pk.gov.pitb.bdas.model.loginResponse.UserData;
import pk.gov.pitb.bdas.serverRetrofit.APIService;
import pk.gov.pitb.bdas.serverRetrofit.ApiUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginViewModel extends ViewModel {

    private MutableLiveData<String> username;
    private MutableLiveData<String> password;

    private List<UserData> userList;

    UserData newUser;

    @SuppressLint("StaticFieldLeak")
    private Activity activity;
    private CallBackFragment callBackFragment;
    private  FragmentLoginBinding binding;
    @SuppressLint("StaticFieldLeak")
    private Context mContext;
    private ProgressDialog progressDialog;
    private Globals mGlobals;
    private APIService mAPIService;
    String recaptchaToken = "";
    //private GoogleApiClient googleApiClient;

    public MutableLiveData<String> getUsername() {
        return username;
    }

    public void setUsername(MutableLiveData<String> username) {
        this.username = username;
    }

    public MutableLiveData<String> getPassword() {
        return password;
    }

    public void setPassword(MutableLiveData<String> password) {
        this.password = password;
    }

    public LoginViewModel(Context context, StartActivity activity, FragmentLoginBinding binding) {

        this.binding = binding;
        this.mContext = context;
        this.callBackFragment = activity;
        this.activity = activity;

        setInstances();

        listeners();
    }

    private void setInstances(){
        mGlobals = Globals.getInstance(activity);
        SugarContext.init(mContext);
        ButterKnife.bind(activity);
        mAPIService = ApiUtils.getAPIService();
        StatusBarUtil.setTransparent(activity);
    }



    private void listeners() {

//        this.binding.etUsername.addTextChangedListener(new TextWatcherUsername(binding.etUsername));

//captcha work start
//        googleApiClient = new GoogleApiClient.Builder(this)
//                .addApi(SafetyNet.API)
//                .addConnectionCallbacks(SignInActivity.this)
//                .build();
//        googleApiClient.connect();
//
//        captcha.setOnCheckedChangeListener((buttonView, isChecked) -> {
//            captcha.setError(null);
//        });
//
//        captcha.setOnClickListener(v -> {
//            if(captcha.isChecked()){
//                SafetyNet.getClient(this).verifyWithRecaptcha(siteKey())
//                        .addOnSuccessListener(recaptchaTokenResponse -> {
//                            //validateCaptcha(recaptchaTokenResponse.getTokenResult());
//                            Toast.makeText(getApplicationContext(), recaptchaTokenResponse.getTokenResult(), Toast.LENGTH_LONG).show();
//                            recaptchaToken = recaptchaTokenResponse.getTokenResult();
//                        })
//                        .addOnFailureListener(e -> {
//                            Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
////                            captcha.setChecked(false);
//                        });
//
//            }
//        });
//captcha work end

        this.binding.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateFields()){
                    login();
                }
            }
        });

    }

    private void clearFields() {
        binding.etUsername.setText("");
        binding.etPassword.setText("");
    }

    private void login(){

        progressDialog = new ProgressDialog(mContext);
        progressDialog.setMessage("Signing in...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("email", binding.etUsername.getText().toString());
            jsonObject.addProperty("password", binding.etPassword.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        Map<String, String> hashMap = new HashMap();
        hashMap.put("data", jsonObject.toString());
        hashMap.put("app_data", Globals.getUsage().getJSON().toString());

        mAPIService.login(hashMap).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    Log.i("TAG", "post submitted to API." + response.body().toString());
                    LoginResponse loginResponse = response.body();
                    DatabaseHelper.resetAllDatabase();
                    if(loginResponse.getStatus()){
                        // login success
//                        Globals.getUsage().sharedPreferencesEditor.setFirstTimeLogin(true);

                        UserData userData = loginResponse.getUserData();
                        FormData formData = loginResponse.getFormData();

                        String accessToken = loginResponse.getAccessToken();
                        userData.setAccessToken(accessToken);
                        if(formData!=null){
                            formData.setUnsent(false);
                            formData.save();
                        }

                        if(userData!=null) {
                            userData.save();
                            Globals.getUsage().sharedPreferencesEditor.setLoggedIn(true);
                            openSplashActivity();
                        }else{
                            SweetAlertDialogs.getInstance().showDialogOK(mGlobals.mContext, "Error", "User Data is not there in response", null, false);
                        }
                    }else{
                        SweetAlertDialogs.getInstance().showDialogOK(mGlobals.mContext, activity.getString(R.string.cannot_signin), loginResponse.getMessage(), null, false);
                    }
                } else {
                    Globals.getUsage().showDialogFailure(Globals.getUsage().mContext.getResources().getString(R.string.try_later));
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                progressDialog.dismiss();
                Log.e("failure", "Unable to submit post to API.");
                if (t instanceof IOException) {
                    Globals.getUsage().showDialogFailure(Globals.getUsage().mContext.getString(R.string.network_error));     // logging probably not necessary
                }
                else {
                    Globals.getUsage().showDialogFailure(Globals.getUsage().mContext.getString(R.string.error_message));
                }
            }
        });

    }

    private boolean validateFields() {

        if(binding.etUsername.length() == 0) {
            binding.etUsername.setError("Field Required");
            return false;
        }
        if(binding.etPassword.length() == 0){
            binding.etPassword.setError("Field Required");
            return false;
        }
        return true;
    }

    public void openSplashActivity(){
        Intent intent = new Intent(activity, DrawerActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
        activity.finish();
    }

}