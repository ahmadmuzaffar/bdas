package pk.gov.pitb.bdas.helperClasses;

/**
 * Created by pitb on 3/30/2016.
 */
public class Constants {


//        public static final String BASE_URL = "http://dev.poltrain.punjabpolice.gov.pk/school_council/api/";
    public static final String BASE_URL = "https://dfd.punjab.gov.pk/api/";


    /* DATE FORMAT */
    public static final String FORMAT_TIME_APP = "HH:mm:ss";
    public static final String FORMAT_DATE_APP = "yyyy-MM-dd";
    public static final String FORMAT_DATE_TIME = "yyyy-MM-dd HH:mm:ss";
    public static final String VIEW_FORMAT_DATE_TIME = "dd-MM-yyyy hh:mm a";

    /*    */

    public static final String SERVER_HEADING = "Server Error";
    public static final String INTERNET_HEADING = "Connection Error";
    public static final String SERVER_DESC = "Invalid response from Server. Please contact admin.";
    public static final String INTERNET_DESC = "No internet connection.";

    public static final String API_KEY = "AIzaSyB9Fk8ihsGMMYKjuPA1t7_JBpejkKalJhA";


    public static final String MEDIA_TYPE_VIDEO = "video";
    public static final String MEDIA_TYPE_ATTACHMENT = "attachment";


    /* DATE LIMITS */
    public static final int LENGTH_CNIC = 15;
    public static final int MIN_LENGTH_MOBILE_NUMBER = 11;
    public static final int MAX_LENGTH_MOBILE_NUMBER = 11;

    /* HANDLER ACTION CODES */
    public static final int AC_DEFAULT = -1;
    public static final int AC_SHOW_DIALOG = 0;
    public static final int AC_INTERNET_NOT_CONNECTED = 1;
    public static final int AC_INTERNET_NOT_WORKING = 2;
    public static final int AC_HTTP_REQUEST_SUCCESS = 3;
    public static final int AC_HTTP_REQUEST_ERROR = 4;
    public static final int AC_HTTP_REQUEST_EXCEPTION = 5;
    public static final int AC_HTTP_REQUEST_DATA_OUTDATED = 6;
    public static final int AC_HTTP_REQUEST_NO_DATA = 7;

    // FACILITY TYPES
    public static final String TYPE_BHU = "1";
    public static final String TYPE_BHU_MORNING = "2";
    public static final String TYPE_RHC_EVENING = "4";
    public static final String TYPE_DHQ = "7";
    public static final String TYPE_THQ = "8";
    public static final String TYPE_CLOSED = "10";
    public static final String TYPE_BHU_EVENING = "11";
    public static final String TYPE_RHC_MORNING = "12";
    public static final String TYPE_MSDS_BHU = "13";
    public static final String TYPE_MSDS_RHC = "14";
    public static final String TYPE_AMBULANCE = "15";
    // SUB TYPES / FORMS
    public static final String SUB_TYPE_STAFF = "1";
    public static final String SUB_TYPE_MEDICINE = "2";
    public static final String SUB_TYPE_MEDICINE_SUPPLIES = "3";
    public static final String SUB_TYPE_EQUIPMENT = "4";
    public static final String SUB_TYPE_NP = "5";
    public static final String SUB_TYPE_DISPLAY = "6";
    public static final String SUB_TYPE_FACILITY_PARAMS = "7";
    public static final String SUB_TYPE_UTILS = "8";
    public static final String SUB_TYPE_DELIVERY = "9";
    public static final String SUB_TYPE_HCP = "10";
    public static final String SUB_TYPE_EPI_VACCINE = "11";
    public static final String SUB_TYPE_PATIENT_DATA = "12";
    public static final String SUB_TYPE_PICTURE_FORM = "13";
    public static final String SUB_TYPE_MSDS_BHU_FORM = "14";
    public static final String SUB_TYPE_MSDS_RHC_FORM = "15";
    public static final String SUB_TYPE_AMBULANCE = "16";

    // FACILITY TYPES
    public static final String TYPE_NAME_BHU = "BHU";
    public static final String TYPE_NAME_BHU_MORNING = "BHU MORNING";
    public static final String TYPE_NAME_BHU_EVENING = "BHU EVENING";
    public static final String TYPE_NAME_RHC_EVENING = "RHC EVENING";
    public static final String TYPE_NAME_RHC_MORNING = "RHC MORNING";
    public static final String TYPE_NAME_THQ = "THQ";
    public static final String TYPE_NAME_DHQ = "DHQ";
    public static final String TYPE_NAME_CLOSED = "CLOSED FACILITY";
    public static final String TYPE_NAME_MSDS_BHU = "MSDS BHU";
    public static final String TYPE_NAME_MSDS_RHC = "MSDS RHC";
    public static final String TYPE_NAME_AMBULANCE = "AMBULANCE";

    // SUB TYPES / FORMS
    public static final String SUB_TYPE_NAME_STAFF = "STAFF FORM";
    public static final String SUB_TYPE_NAME_MEDICINE = "MEDICINE AVAILABILITY COUNT";
    public static final String SUB_TYPE_NAME_MEDICINE_SUPPLIES = "MEDICAL SUPPLIES";
    public static final String SUB_TYPE_NAME_EQUIPMENT = "MEDICAL EQUIPMENT";
    public static final String SUB_TYPE_NAME_NP = "NATIONAL PROGRAM";
    public static final String SUB_TYPE_NAME_DISPLAY = "DISPLAY FORM";
    public static final String SUB_TYPE_NAME_FACILITY_PARAMS = "FACILITY CONDITION";
    public static final String SUB_TYPE_NAME_UTILS = "UTILITIES";
    public static final String SUB_TYPE_NAME_DELIVERY = "DELIVERY DATA";
    public static final String SUB_TYPE_NAME_HCP = "HEPATITIS & INFECTION CONTROL";
    public static final String SUB_TYPE_NAME_EPI_VACCINE = "EPI VACCINE";
    public static final String SUB_TYPE_NAME_PATIENT_DATA = "PATIENT DATA";
    public static final String SUB_TYPE_NAME_PICTURE_FORM = "FACILITY PICTURE";
    public static final String SUB_TYPE_NAME_MSDS_BHU_FORM = "MSDS BHU";
    public static final String SUB_TYPE_NAME_MSDS_RHC_FORM = "MSDS RHC";
    public static final String SUB_TYPE_NAME_AMBULANCE = "AMBULANCE CHECKLIST";

    // view type

    public static final String TYPE_INPUT_TEXT = "text";
    public static final String TYPE_INPUT_NUMBER = "number";
    public static final String TYPE_INPUT_PHONE = "phone";
    public static final String TYPE_DATE = "date";
    public static final String TYPE_DROPDOWN = "dropdown";
    public static final String TYPE_DROPDOWN_MULTIPLE = "multiple_dropdown";
    public static final String TYPE_PICTURE = "picture";
    public static final String TYPE_HEADING = "heading";
    public static final String TYPE_SUB_HEADING = "sub-heading";

    //action type values
    public static final String ACTION_HIDE = "hide";
    public static final String ACTION_SHOW = "show";
    public static final String ACTION_NONE = "none";


}
