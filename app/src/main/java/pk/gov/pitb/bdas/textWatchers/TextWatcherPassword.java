package pk.gov.pitb.bdas.textWatchers;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class TextWatcherPassword implements TextWatcher {

    private EditText editText = null;

    public TextWatcherPassword(EditText editText) {
        this.editText = editText;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if(s.length() == 8){
            editText.setError(null);
        }
    }
}
