package pk.gov.pitb.bdas.model.loginResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

public class FormData extends SugarRecord {

    @SerializedName("form_id")
    @Expose
    private Integer formId;
    @SerializedName("pending_building")
    @Expose
    private Integer pendingBuilding;
    @SerializedName("new_building")
    @Expose
    private Integer newBuilding;
    @SerializedName("total_building")
    @Expose
    private Integer totalBuilding;
    @SerializedName("approved_building")
    @Expose
    private Integer approvedBuilding;
    @SerializedName("remaining_building")
    @Expose
    private Integer remainingBuilding;
    @SerializedName("division_id")
    @Expose
    private Integer divisionId;
    @SerializedName("district_id")
    @Expose
    private Integer districtId;
    @SerializedName("local_govt")
    @Expose
    private Integer localGovt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("created_by")
    @Expose
    private Integer createdBy;
    @SerializedName("updated_by")
    @Expose
    private Integer updatedBy;
    @SerializedName("is_deleted")
    @Expose
    private Integer isDeleted;
    @SerializedName("is_unsent")
    @Expose
    private boolean isUnsent = false;

    public Integer getFormId() {
        return formId;
    }

    public void setFormId(Integer formId) {
        this.formId = formId;
    }

    public Integer getPendingBuilding() {
        return pendingBuilding;
    }

    public void setPendingBuilding(Integer pendingBuilding) {
        this.pendingBuilding = pendingBuilding;
    }

    public Integer getNewBuilding() {
        return newBuilding;
    }

    public void setNewBuilding(Integer newBuilding) {
        this.newBuilding = newBuilding;
    }

    public Integer getTotalBuilding() {
        return totalBuilding;
    }

    public void setTotalBuilding(Integer totalBuilding) {
        this.totalBuilding = totalBuilding;
    }

    public Integer getApprovedBuilding() {
        return approvedBuilding;
    }

    public void setApprovedBuilding(Integer approvedBuilding) {
        this.approvedBuilding = approvedBuilding;
    }

    public Integer getRemainingBuilding() {
        return remainingBuilding;
    }

    public void setRemainingBuilding(Integer remainingBuilding) {
        this.remainingBuilding = remainingBuilding;
    }

    public Integer getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(Integer divisionId) {
        this.divisionId = divisionId;
    }

    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    public Integer getLocalGovt() {
        return localGovt;
    }

    public void setLocalGovt(Integer localGovt) {
        this.localGovt = localGovt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }


    public boolean isUnsent() {
        return isUnsent;
    }

    public void setUnsent(boolean unsent) {
        isUnsent = unsent;
    }
}
