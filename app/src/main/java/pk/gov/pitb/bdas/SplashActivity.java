package pk.gov.pitb.bdas;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.gson.JsonObject;
import com.jaeger.library.StatusBarUtil;
import com.orm.SugarContext;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import pk.gov.pitb.bdas.dialogues.SweetAlertDialogs;
import pk.gov.pitb.bdas.helperClasses.Globals;
import pk.gov.pitb.bdas.model.SubmitResponse;
import pk.gov.pitb.bdas.model.loginResponse.FormData;
import pk.gov.pitb.bdas.model.loginResponse.UserData;
import pk.gov.pitb.bdas.requestPermisson.RequestPermissionsHelper;
import pk.gov.pitb.bdas.serverRetrofit.APIService;
import pk.gov.pitb.bdas.serverRetrofit.ApiUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    public static final int PERMISSION_REQUEST_CODE = 0;
    Thread thread=null;
    List<FormData> formDataUnsent;
    UserData userData;
    private APIService mAPIService;

    // Used to load the 'native-lib' library onoldAPIsURL application startup.
    static {
        System.loadLibrary("native-lib");
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        requestPermission();
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Android M Permission check
            int permission = RequestPermissionsHelper.requestPermission(SplashActivity.this);
            if(permission == RequestPermissionsHelper.requestPermissionCode){
                ActivityCompat.requestPermissions(SplashActivity.this, RequestPermissionsHelper.persmissionsStringArray, SplashActivity.PERMISSION_REQUEST_CODE);
            }else if(permission == RequestPermissionsHelper.notShouldShowRequestPermissionRationaleCode){
                showDialogPermissionDenied();
            } if(permission == RequestPermissionsHelper.notCheckSelfPermissionCode) {
                initializeApplication();
            }
        } else {
            initializeApplication();
        }
    }

    private void showDialogPermissionDenied() {
        SweetAlertDialogs.getInstance().showDialogPermissionDenied(this, new SweetAlertDialogs.OnDialogClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                finish();
            }
        });
    }
    public void setInstances(){
        Globals.getInstance(this);
        SugarContext.init(this);
        StatusBarUtil.setTransparent(this);
        mAPIService = ApiUtils.getAPIService();

    }
    private void initializeApplication() {
        setInstances();

        formDataUnsent = FormData.find(FormData.class,"is_unsent = ?","1");

        if(Globals.getUsage().sharedPreferencesEditor.IsLoggedIn()){
            if(formDataUnsent.size()>0){
                if(Globals.getUsage().isInternetAvailable()){
                    sendUnsent();
                }else{
                    setToWait();
                }
            }else {
                setToWait();
            }
        }else{
            startLoginActivity();
        }
    }

    private void setToWait() {
        thread = new Thread() {
            @Override
            public void run() {
                try {
                    sleep(1000);

                    startMainActivity();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }
    private void startLoginActivity() {
        Intent intent = new Intent(SplashActivity.this, StartActivity.class);
        startActivity(intent);
        finish();
    }
    private void startMainActivity() {
        Intent intent = new Intent(SplashActivity.this, DrawerActivity.class);
        startActivity(intent);
        finish();
    }


    private ProgressDialog progressDialog;
    int unsentTreverser = 0;

    public void sendUnsent() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Sending unsent Data");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        userData = UserData.last(UserData.class);

        submit();

    }
    private void submit() {
        progressDialog.setTitle("Sending unsent Data " + (unsentTreverser + 1) + " of " + formDataUnsent.size());

        final FormData formDataObject = formDataUnsent.get(unsentTreverser);

        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("user_id", userData.getUserId());
            jsonObject.addProperty("user_token", userData.getAccessToken());
            jsonObject.addProperty("pending_building", formDataObject.getPendingBuilding());
            jsonObject.addProperty("new_building", formDataObject.getNewBuilding());
            jsonObject.addProperty("total_building", formDataObject.getTotalBuilding());
            jsonObject.addProperty("approved_building", formDataObject.getApprovedBuilding());
            jsonObject.addProperty("remaining_building", formDataObject.getRemainingBuilding());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("create json string",jsonObject.toString());


        Map<String, String> hashMap = new HashMap();
        hashMap.put("data", jsonObject.toString());
        hashMap.put("app_data", Globals.getUsage().getJSON().toString());


        mAPIService.saveBuilding("Bearer " + userData.getAccessToken(), hashMap)
                .enqueue(new Callback<SubmitResponse>() {
                    @Override
                    public void onResponse(Call<SubmitResponse> call, Response<SubmitResponse> response) {
                        progressDialog.dismiss();
                        if (response.isSuccessful()) {
                            Log.i("TAG", "post submitted to API." + response.body().toString());

                            SubmitResponse submitResponse = response.body();
                            if(submitResponse.getStatus()){
                                // success
                                try {


                                    unsentTreverser++;
                                    if (unsentTreverser < formDataUnsent.size()) {
                                        //del submitted from db
                                        formDataObject.delete();

                                        submit();
                                    }else {
                                        FormData.deleteAll(FormData.class, "is_unsent =?","0");
                                        formDataObject.delete();
                                        formDataObject.setUnsent(false);
                                        formDataObject.save();
                                        progressDialog.dismiss();
                                        setToWait();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }else{
                                SweetAlertDialogs.getInstance().showDialogOK(Globals.getUsage().mContext, "Cannot Submit unsent", submitResponse.getMessage(), new SweetAlertDialogs.OnDialogClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();
                                        finish();
                                    }
                                }, false);
                            }
                        } else {
                            Globals.getUsage().showDialogFailure(Globals.getUsage().mContext.getResources().getString(R.string.try_later));
                        }
                    }

                    @Override
                    public void onFailure(Call<SubmitResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        Log.e("failure", "Unable to submit post to API.");
                        if (t instanceof IOException) {
                            SweetAlertDialogs.getInstance().showDialogOK(Globals.getUsage().mContext, "Network Error",
                                    Globals.getUsage().mContext.getString(R.string.network_error_save), new SweetAlertDialogs.OnDialogClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                            finish();
                                        }
                                    }, false);
                        }
                        else {
                            SweetAlertDialogs.getInstance().showDialogOK(Globals.getUsage().mContext, "Error",
                                    Globals.getUsage().mContext.getString(R.string.error_message), new SweetAlertDialogs.OnDialogClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                            finish();
                                        }
                                    }, false);
                        }
                    }
                });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[],
                                           int[] grantResults) {
        switch (requestCode) {
            case SplashActivity.PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0){
                    boolean granted = true;
                    for(int i=0;i<grantResults.length;i++){
                        if(grantResults[i] != PackageManager.PERMISSION_GRANTED){
                            granted=false;
                            break;
                        }
                    }
                    if(granted){
                        initializeApplication();
                    }else{
                        showDialogPermissionDenied();
                    }
                } else {
                    showDialogPermissionDenied();
                }
                break;
        }
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public static native String baseURL();


}
