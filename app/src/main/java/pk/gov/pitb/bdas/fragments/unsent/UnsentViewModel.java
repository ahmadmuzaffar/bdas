package pk.gov.pitb.bdas.fragments.unsent;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import pk.gov.pitb.bdas.adapters.ItemAdapter;
import pk.gov.pitb.bdas.databinding.FragmentUnsentBinding;
import pk.gov.pitb.bdas.model.loginResponse.FormData;

public class UnsentViewModel extends ViewModel {

    @SuppressLint("StaticFieldLeak")
    private Activity activity;
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private FragmentUnsentBinding binding;

    RecyclerView rv;
    RecyclerView.LayoutManager layoutManager;
    List<FormData> itemList;
    ItemAdapter itemAdapter;

    public UnsentViewModel(FragmentActivity activity, Context context, FragmentUnsentBinding binding) {

        this.activity = activity;
        this.context = context;
        this.binding = binding;

        initializeRecyclerView();

    }

    private void initializeRecyclerView() {
        rv = binding.rvUnsent;
        layoutManager = new LinearLayoutManager(activity);
        rv.setLayoutManager(layoutManager);
        itemList = FormData.find(FormData.class,"is_unsent = ?",""+1);
        itemAdapter = new ItemAdapter(itemList);
        rv.setAdapter(itemAdapter);
    }
}
