package pk.gov.pitb.bdas.serverRetrofit;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static pk.gov.pitb.bdas.SplashActivity.baseURL;

/**
 * Created by PITB on 10/18/2017.C
 */

public class RetrofitClient {

    private static Retrofit retrofit = null;
    private APIService apiService;

//    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
//            .connectTimeout(10, TimeUnit.MINUTES)
//            .readTimeout(10, TimeUnit.MINUTES)
//            .writeTimeout(10, TimeUnit.MINUTES);
//
//    private static Gson gson = new GsonBuilder()
//            .setLenient()
//            .create();
//
//    public static Retrofit getClient(String baseUrl) {
//        if (retrofit==null) {
//            retrofit = new Retrofit.Builder()
//                    .baseUrl(baseUrl)
//                    .addConverterFactory(GsonConverterFactory.create())
//                    .client(httpClient.build())
//                    .build();
//        }
//        return retrofit;
//    }


    public RetrofitClient() {
        SSLContext sslContext = null;
        try {
            sslContext = SSLContext.getInstance("TLSv1.2");
            sslContext.init(null, null, null);
            OkHttpClient httpClient = new OkHttpClient.Builder()
                    .readTimeout(120, TimeUnit.SECONDS)
                    .connectTimeout(120, TimeUnit.SECONDS)
                    .retryOnConnectionFailure(true)
                    .sslSocketFactory(sslContext.getSocketFactory())
                    .addInterceptor(new LoggingInterceptor()).build();

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .setDateFormat("'yyyy'-'MM'-'dd' 'HH':'mm':'ss'")
                    .create();

            if (retrofit==null) {
                retrofit = new Retrofit.Builder()
                        .baseUrl(baseURL())
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .client(httpClient)
                        .build();
            }
            apiService = retrofit.create(APIService.class);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
    }


    public class LoggingInterceptor implements Interceptor {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            long t1 = System.nanoTime();
            String requestLog = String.format("Sending request %s on %s%n%s", request.url(), chain.connection(), request.headers());
            if (request.method().compareToIgnoreCase("post") == 0) {
                requestLog = "\n" + requestLog + "\n" + bodyToString(request);
            }
            Log.d("Retrofit", "request" + "\n" + requestLog);
            Response response = chain.proceed(request);
            long t2 = System.nanoTime();
            String responseLog = String.format("Received response for %s in %.1fms%n%s", response.request().url(), (t2 - t1) / 1e6d, response.headers());
            String bodyString = response.body().string();
            Log.d("Retrofit", "response" + "\n" + responseLog + "\n" + bodyString);
            return response.newBuilder().body(ResponseBody.create(response.body().contentType(), bodyString)).build();
        }
    }

    public String bodyToString(final Request request) {
        try {
            final Request copy = request.newBuilder().build();
            final Buffer buffer = new Buffer();
            copy.body().writeTo(buffer);
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }

    public APIService getApiService() {
        return apiService;
    }


}