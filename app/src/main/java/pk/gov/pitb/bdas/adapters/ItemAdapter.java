package pk.gov.pitb.bdas.adapters;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import pk.gov.pitb.bdas.R;
import pk.gov.pitb.bdas.model.loginResponse.FormData;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.MyViewHolder> {

    private List<FormData> mDataset;

    public ItemAdapter(List<FormData> myDataSet) {
        mDataset = myDataSet;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.recycler_view_items, parent, false);
        return  new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.textViewPending.setText("Pending Plans: " + mDataset.get(position).getPendingBuilding());
        holder.textViewNew.setText("New Added Plans: " + mDataset.get(position).getNewBuilding());
        holder.textViewTotal.setText("Total Plans: " + mDataset.get(position).getTotalBuilding());
        holder.textViewApproved.setText("Approved Plans: " + mDataset.get(position).getApprovedBuilding());
        holder.textViewRemaining.setText("Remaining Plans: " + mDataset.get(position).getRemainingBuilding());
//        holder.txt5.setText("Date: " + mDataset.get(position).getDate());
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{

        TextView textViewPending;
        TextView textViewNew;
        TextView textViewTotal;
        TextView textViewApproved;
        TextView textViewRemaining;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewPending = itemView.findViewById(R.id.pending);
            textViewNew = itemView.findViewById(R.id.newAdd);
            textViewTotal = itemView.findViewById(R.id.total);
            textViewApproved = itemView.findViewById(R.id.approved);
            textViewRemaining = itemView.findViewById(R.id.remaining);
        }
    }
}
