package pk.gov.pitb.bdas.fragments.home;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pk.gov.pitb.bdas.DrawerActivity;
import pk.gov.pitb.bdas.R;
import pk.gov.pitb.bdas.databinding.FragmentHomeBinding;

public class HomeFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        FragmentHomeBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        HomeViewModel homeViewModel = new HomeViewModel(getContext(), binding, (DrawerActivity) getActivity());
        binding.setHomeViewModel(homeViewModel);

        return binding.getRoot();
    }
}
