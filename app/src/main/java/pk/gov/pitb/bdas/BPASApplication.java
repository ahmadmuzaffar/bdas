package pk.gov.pitb.bdas;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.os.Bundle;

import com.orm.SugarContext;

import java.io.File;

import pk.gov.pitb.bdas.helperClasses.TypeFaceUtil;


public class BPASApplication extends Application {

    public static String TAG = "BuildingPlanApplication";
    private static BPASApplication instance;
    public static Context context;
    private static File cacheDir;
    private Activity activeActivity;
    public static MediaPlayer mediaPlayer;
    public static Boolean streamState = Boolean.valueOf(true);


    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
//        MultiDex.install(this);
        instance = this;
        BPASApplication.context = getApplicationContext();
        TypeFaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Jameel_Noori_Nastaleeq.ttf");
        setupActivityListener();

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        SugarContext.init(this);
    }

    public BPASApplication() {
    }

    public static BPASApplication getInstance() {
        return instance;
    }

    public static Context getAppContext() {
        return instance;
    }

    public static Resources getAppResources() {
        return instance.getResources();
    }

    public static Application getApplicationInstance() {
        return instance;
    }

    private void setupActivityListener() {
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
            }

            @Override
            public void onActivityStarted(Activity activity) {
            }

            @Override
            public void onActivityResumed(Activity activity) {
                activeActivity = activity;
            }

            @Override
            public void onActivityPaused(Activity activity) {
//                final Intent serviceIntent = new Intent(BPASApplication.context, SpentTimeService.class);
//                startService(serviceIntent);
                activeActivity = null;
            }

            @Override
            public void onActivityStopped(Activity activity) {
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
            }

            @Override
            public void onActivityDestroyed(Activity activity) {
            }
        });
    }

    public Activity getActiveActivity() {
        return activeActivity;
    }
}