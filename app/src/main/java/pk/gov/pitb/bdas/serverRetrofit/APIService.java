package pk.gov.pitb.bdas.serverRetrofit;

import java.util.Map;

import pk.gov.pitb.bdas.model.SubmitResponse;
import pk.gov.pitb.bdas.model.loginResponse.LoginResponse;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by PITB on 10/18/2017.
 */

public interface APIService {

    @POST("login")
    @FormUrlEncoded
    Call<LoginResponse> login(@FieldMap Map<String, String> data);

    @POST("updatePassword")
    @FormUrlEncoded
    Call<LoginResponse> changePassword(@Header("Authorization") String auth,
                                         @FieldMap Map<String, String> data);

    @POST("store_data")
    @FormUrlEncoded
    Call<SubmitResponse> saveBuilding(@Header("Authorization") String auth,
                                      @FieldMap Map<String, String> data);




}
