#include <jni.h>
#include <string>


extern "C"
JNIEXPORT jstring
JNICALL
Java_pk_gov_pitb_bdas_SplashActivity_baseURL(JNIEnv *env, jobject /* this */) {
    std::string url = "http://dev.poltrain.punjabpolice.gov.pk/pmsp/public/api/";
    return env->NewStringUTF(url.c_str());
}